﻿using Pic2Pointcloud.Annotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Pic2Pointcloud
{
    public class ProgressBarClass : INotifyPropertyChanged
    {
        private Visibility _barVisibility = Visibility.Collapsed;
        private int _currentValue;
        private int _maxValue;

        public Visibility BarVisibility
        {
            get => _barVisibility;
            set
            {
                if (value == _barVisibility) return;
                _barVisibility = value;
                OnPropertyChanged();
            }
        }

        public int CurrentValue
        {
            get => _currentValue;
            set
            {
                if (value == _currentValue) return;
                _currentValue = value;
                OnPropertyChanged();
            }
        }

        public int MaxValue
        {
            get => _maxValue;
            set
            {
                if (value == _maxValue) return;
                _maxValue = value;
                OnPropertyChanged();
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}