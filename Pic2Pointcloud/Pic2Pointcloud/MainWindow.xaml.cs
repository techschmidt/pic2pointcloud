﻿using System;
using Microsoft.Win32;
using Pic2Pointcloud.Annotations;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Pic2Pointcloud
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private ProgressBarClass _allItemsBar = new ProgressBarClass();
        private bool _openIsEnabled = true;
        private ObservableCollection<string> _picPathList = new ObservableCollection<string>();
        private ProgressBarClass _singleItmenBar = new ProgressBarClass();
        private bool _startIsEnabled;

        public ProgressBarClass AllItemsBar
        {
            get => _allItemsBar;
            set
            {
                if (Equals(value, _allItemsBar)) return;
                _allItemsBar = value;
                OnPropertyChanged();
            }
        }

        public bool OpenIsEnabled
        {
            get => _openIsEnabled;
            set
            {
                if (value == _openIsEnabled) return;
                _openIsEnabled = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<string> PicPathList
        {
            get => _picPathList;
            set
            {
                if (Equals(value, _picPathList)) return;
                _picPathList = value;
                OnPropertyChanged();
            }
        }

        public ProgressBarClass SingleItmenBar
        {
            get => _singleItmenBar;
            set
            {
                if (Equals(value, _singleItmenBar)) return;
                _singleItmenBar = value;
                OnPropertyChanged();
            }
        }

        public bool StartIsEnabled
        {
            get => _startIsEnabled;
            set
            {
                if (value == _startIsEnabled) return;
                _startIsEnabled = value;
                OnPropertyChanged();
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private string ConvertPic(string path)
        {
            var coordinateStringBuilder = new StringBuilder();
            using (var pic = new Bitmap(path))
            {
                SingleItmenBar.MaxValue = pic.Height * pic.Width;
                for (var i = 0; i < pic.Height; i++)
                {
                    for (var j = 0; j < pic.Width; j++)
                    {
                        var clr = pic.GetPixel(j, i);
                        if (clr.R == 0 && clr.G == 0 && clr.B == 0)
                        {
                            SingleItmenBar.CurrentValue++;
                            continue;
                        }
                        coordinateStringBuilder.AppendLine($"{i} {j} {(int)clr.R + (int)clr.G + (int)clr.B}");
                        SingleItmenBar.CurrentValue++;
                    }

                    SingleItmenBar.CurrentValue++;
                }
            }
            SingleItmenBar.CurrentValue = 0;
            return coordinateStringBuilder.ToString();
        }

        private void OnOpenClick(object sender, RoutedEventArgs e)
        {
            PicPathList.Clear();
            var openFileDialog = new OpenFileDialog
            {
                Filter = "Bitmap (*.bmp)|*.bmp",
                Multiselect = true
            };
            if (openFileDialog.ShowDialog() != true) return;
            foreach (var file in openFileDialog.FileNames)
            {
                PicPathList.Add(file);
            }
            StartIsEnabled = true;
        }

        private async void OnStartClick(object sender, RoutedEventArgs e)
        {
            OperationStarted();
            if (PicPathList.Count > 1)
            {
                AllItemsBar.MaxValue = PicPathList.Count;
                AllItemsBar.CurrentValue = 0;
                AllItemsBar.BarVisibility = Visibility.Visible;
            }

            await Task.Run(() =>
                           {
                               SingleItmenBar.BarVisibility = Visibility.Visible;
                               foreach (var pic in PicPathList)
                               {
                                   var newPath = pic.Replace("bmp", "asc");
                                   try
                                   {
                                       File.WriteAllText(newPath, ConvertPic(pic));
                                   }
                                   catch (Exception exception)
                                   {
                                       MessageBox.Show($"Fehler beim schreiben:\n" +
                                                       $"{newPath}\n" +
                                                       $"{exception.Message}");
                                       break;
                                   }

                                   AllItemsBar.CurrentValue++;
                               }
                           });
            OperationFinished();
        }

        private void OperationFinished()
        {
            OpenIsEnabled = true;
            StartIsEnabled = false;
            AllItemsBar.BarVisibility = Visibility.Collapsed;
            SingleItmenBar.BarVisibility = Visibility.Collapsed;
            MessageBox.Show("Vorgang wurde erfolgreich beendet.", "Vorgang beendet", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void OperationStarted()
        {
            OpenIsEnabled = false;
            StartIsEnabled = false;
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}